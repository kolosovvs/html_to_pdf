FROM python:3.6-alpine
RUN apk upgrade && apk add python3-dev && apk add g++
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
WORKDIR /app/html_to_pdf
CMD python3.6 main.py 0.0.0.0 5000 athenapdf 8080