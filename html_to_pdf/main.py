"""
Usage:
  main.py <app-host> <app-port> <pdf-host> <pdf-port>
"""

from aiohttp import web
from docopt import docopt
from config import REDIS_HOST
from routes import setup_routes
import asyncio
import aioredis


async def init_app(loop, pdfhost, pdfport):
    app = web.Application(loop=loop)
    setup_routes(app)
    redis = await aioredis.create_redis(f'redis://{REDIS_HOST}', loop=loop)
    app.redis = redis
    app.pdfhost = pdfhost
    app.pdfport = pdfport
    return app


if __name__ == '__main__':
    arguments = docopt(__doc__, version='1.0.0rc2')
    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(init_app(loop, arguments['<pdf-host>'], arguments['<pdf-port>']))
    web.run_app(app, host=arguments['<app-host>'], port=arguments['<app-port>'])
