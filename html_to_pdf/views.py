import uuid
import aiohttp
from aiohttp import web
import asyncio
import aioredis
from config import API_URL


async def generate_pdf(request):
    if request.body_exists:
        html = await request.read()
        redis = request.app.redis
        uid = str(uuid.uuid4())
        await redis.set(uid, html, expire=60)
        url = f'http://{request.app.pdfhost}:{request.app.pdfport}/convert?auth=arachnys-weaver&url={API_URL}/raw/{uid}'
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as resp:
                await redis.delete(uid)
                pdf = await resp.read()
                return web.Response(body=pdf, content_type='application/pdf')


async def get_html(request):
    uuid = request.match_info['uuid']
    if uuid:
        redis = request.app.redis
        html = await redis.get(uuid)
        if html:
            return web.Response(body=html)
