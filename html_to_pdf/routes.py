from aiohttp import web
from views import generate_pdf, get_html


def setup_routes(app):
    app.router.add_routes([web.post('/generate', generate_pdf),
                           web.get('/raw/{uuid}', get_html)],
                          )
