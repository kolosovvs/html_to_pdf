from setuptools import setup, find_packages

setup(
    name='html_to_pdf',
    version='1.0',
    description='Service for converting html to pdf',
    author='Vladimir Kolosov',
    author_email='kolosovvs@gmail.com',
    packages=find_packages(),
    install_requires=['aioredis==1.2.0', 'aiohttp==3.4.4', 'docopt==0.6.2'],
)